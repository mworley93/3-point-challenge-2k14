# 3-Point Challenge 2K14 #

This is a small Android basketball game developed as a final project for CS 335 at the University of Kentucky.  

## What's it About? ##

Step into the shoes of a college basketball player.  How many shots can you make?  Adjust your aim and use the Android motion controls to power your shot.  A free-roaming camera allows you to view your awesome shots from any angle -- and in slow motion!

## Screenshots ##

![Screenshot_2015-01-18-20-04-03.png](https://bitbucket.org/repo/xeBGar/images/3241277897-Screenshot_2015-01-18-20-04-03.png)
![Screenshot_2015-01-18-20-05-28.png](https://bitbucket.org/repo/xeBGar/images/3311968333-Screenshot_2015-01-18-20-05-28.png)
![Screenshot_2015-01-18-20-05-56.png](https://bitbucket.org/repo/xeBGar/images/565115958-Screenshot_2015-01-18-20-05-56.png)
![Screenshot_2015-01-18-20-07-24.png](https://bitbucket.org/repo/xeBGar/images/1849985426-Screenshot_2015-01-18-20-07-24.png)

## Implementation Details
The project build target was Android 4.2.2.  The JPCT-AE engine was used for loading the assets into the game.

## Known Issues ##
The project was developed in a limited amount of time.  While we tried to polish it as much as possible before the deadline, there are a couple of bugs to take note of.

* Switching from portrait to landscape mode or visa versa causes a crash.
* Pressing the GUI controls while the assets are loading causes a crash.
 
## Contributors ##

* Megan Worley
* Carl Groathouse