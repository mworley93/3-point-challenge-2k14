package com.example.virtualbasketball;

import java.lang.reflect.Field;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.test2_3d.*;
import com.threed.jpct.Logger;

/**
 * A simple demo. This shows more how to use jPCT-AE than it shows how to write
 * a proper application for Android. It includes basic activity management to
 * handle pause and resume...
 * 
 * @author EgonOlsen
 * 
 */
public class MainActivity extends Activity implements SensorEventListener {

	CustomGLSurfaceView mGLView;
	ImageButton mUpViewButton;
	ImageButton mLeftViewButton;
	ImageButton mDownViewButton;
	ImageButton mRightViewButton;
	ImageButton mLeftMoveButton;
	ImageButton mRightMoveButton;
	ImageButton mReplayButton;
	Button mSpeedUpButton;
	Button mSlowDownButton;
	Button mResetButton;
	SensorManager mSensorManager;
	private static final float ACCELERATION_THRESHOLD = 1f;
	private static final float SPEED_RATIO = 4f;
	private static final float MAX_SPEED = 10f;
	private ReplaySpeed mReplayMode;
	
	// Used to handle pause and resume...
	private static MainActivity master = null;


	protected void onCreate(Bundle savedInstanceState) {

		Logger.log("onCreate");

		if (master != null) {
			copy(master);
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mGLView = (CustomGLSurfaceView)findViewById(R.id.glView);
		initializeButtons();
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mReplayMode = ReplaySpeed.NORMAL;
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
		mGLView.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this,
		        mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
		        SensorManager.SENSOR_DELAY_NORMAL);
		mGLView.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
    protected void onDestroy() {
    super.onDestroy();

    unbindDrawables(findViewById(R.id.glView));
    System.gc();
    }

    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
        view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
            unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
        ((ViewGroup) view).removeAllViews();
        }
    }
    
	/**
	 * Initializes the buttons for the game.
	 */
	private void initializeButtons() {
		mUpViewButton = (ImageButton)findViewById(R.id.up_view_button);
	    mUpViewButton.setOnTouchListener(new RepeatListener(10,10, new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().changeCamera(ViewpointChange.VIEW_UP);
			}
	    }));
		
		mLeftViewButton = (ImageButton)findViewById(R.id.left_view_button);
		mLeftViewButton.setOnTouchListener(new RepeatListener(10,10, new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().changeCamera(ViewpointChange.VIEW_LEFT);
			}
	    }));
		mDownViewButton = (ImageButton)findViewById(R.id.down_view_button);
		mDownViewButton.setOnTouchListener(new RepeatListener(10,10, new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().changeCamera(ViewpointChange.VIEW_DOWN);
			}
	    }));
		mRightViewButton = (ImageButton)findViewById(R.id.right_view_button);
		mRightViewButton.setOnTouchListener(new RepeatListener(10,10, new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().changeCamera(ViewpointChange.VIEW_RIGHT);
			}
	    }));
		mLeftMoveButton = (ImageButton)findViewById(R.id.left_move_button);
		mLeftMoveButton.setOnTouchListener(new RepeatListener(10,10, new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().changeCamera(ViewpointChange.MOVE_LEFT);
			}
	    }));
		mRightMoveButton = (ImageButton)findViewById(R.id.right_move_button);
		mRightMoveButton.setOnTouchListener(new RepeatListener(10,10, new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().changeCamera(ViewpointChange.MOVE_RIGHT);
			}
	    }));
		mReplayButton = (ImageButton)findViewById(R.id.replay_button);
		mReplayButton.setOnClickListener(new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().replay(mReplayMode);
			}
	    });
		mSpeedUpButton = (Button)findViewById(R.id.increase_button);
		mSpeedUpButton.setOnClickListener(new OnClickListener() {
	    	@Override
			public void onClick(View v) {
	    		mReplayMode = mReplayMode.increase();
				Toast.makeText(MainActivity.this, "Replay speed is now: " + mReplayMode.value(), 
						Toast.LENGTH_SHORT).show();
			}
	    });
		mSlowDownButton = (Button)findViewById(R.id.decrease_button);
		mSlowDownButton.setOnClickListener(new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mReplayMode = mReplayMode.decrease();
				Toast.makeText(MainActivity.this, "Replay speed is now: " + mReplayMode.value(), 
						Toast.LENGTH_SHORT).show();
			}
	    });
		mResetButton = (Button)findViewById(R.id.reset_button);
		mResetButton.setOnClickListener(new OnClickListener() {
	    	@Override
			public void onClick(View v) {
				mGLView.getRenderer().reset();
			}
	    });
	}
	
	  @Override
	  public void onSensorChanged(SensorEvent event) {
	    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
	      getAccelerometer(event);
	    }
	  }
	  
	  /**
	   * http://www.vogella.com/articles/AndroidSensor/article.html
	   * @param event
	   */
	  private void getAccelerometer(SensorEvent event) {
		    float[] values = event.values;
		    // Movement
		    float x = values[0];
		    float y = values[1];
		    float z = values[2];

		    if (z < 0) {
			    float acceleration = (x * x + y * y + z * z)
			        / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
			    if (acceleration >= ACCELERATION_THRESHOLD) {
			    	float speed = acceleration * SPEED_RATIO;
			    	mGLView.getRenderer().throwBall(Math.min(speed, MAX_SPEED));
			    }
		    }
		  }
	  
	  @Override
	  public void onAccuracyChanged(Sensor sensor, int accuracy) {
	  }

	private void copy(Object src) {
		try {
			Logger.log("Copying data from master Activity!");
			Field[] fs = src.getClass().getDeclaredFields();
			for (Field f : fs) {
				f.setAccessible(true);
				f.set(this, f.get(src));
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected boolean isFullscreenOpaque() {
		return true;
	}

}