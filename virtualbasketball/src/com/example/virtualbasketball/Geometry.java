package com.example.virtualbasketball;

/**
 * This class acts as a superclass for some of the data representations for geometric 
 * figures.
 * @author Megan Worley
 */
public abstract class Geometry {
	protected Vector mCenter;
	protected Vector mScale;
	
	/**
	 * Default constructor.
	 * @param center Center location.

	 */
	public Geometry(Vector center) {
		mCenter = center;
	}
	
	public Vector getCenter() {
		return mCenter;
	}
	
	public Vector getScale() {
		return mScale;
	}
}
