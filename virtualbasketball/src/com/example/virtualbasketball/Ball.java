package com.example.virtualbasketball;

import java.util.ArrayList;

/**
 * This class represents the basketball in the game.  It keeps track of the balls position, 
 * movement, scoring status, and handles any collisions.
 * @author Megan Worley
 *
 */
public class Ball extends Geometry {
	private Vector mVelocity;
	private float mRadius;
	private boolean mRolling;
	private boolean mIsHeld;
	private boolean mReplaying;
	private boolean mRecording;
	private ArrayList<Vector> mRecordedPos;
	private ArrayList<Vector> mPlayback;
	
	// Scoring status variables.
	private boolean mMovedThroughTop;
	private boolean mMovedThroughBot;
	
	// Physics constants.
	private final static float GRAVITY = -0.40f;
	private final static float LOSS_FLOOR_ENERGY = 0.7f;
	private final static float LOSS_GOAL_ENERGY = 0.9f;
	private final static float BOUNCE_THRESHOLD = 5f;
	private final static float FRICTION = 0.99f;
	private final static float FRICTION_THRESHOLD = 0.01f;
	
	/**
	 * Main constructor.
	 * @param center The center position of the ball. 
	 * @param scale The scale of the ball.
	 * @param radius The radius of the ball.
	 */
	public Ball(Vector center, float radius) {
		super(center);
		mRadius = radius;
		mVelocity = new Vector(0,0,0);
		mRolling = false;
		mIsHeld = true;
		resetScoreStatus();
		mReplaying = false;
		mRecordedPos = new ArrayList<Vector>();
		mRecordedPos.add(center);
		mRecording = false;
		System.out.println("constructor");
	}
	
	public float getRadius() {
		return mRadius;
	}
	
	public void setVelocity(float x, float y, float z) {
		mVelocity.set(x, y, z);
	}
	
	public Vector getVelocity() {
		return mVelocity;
	}
	
	public void setCenter(float x, float y, float z) {
		mCenter.set(x, y, z);
	}
	
	public void setMovedThroughHoop(boolean top, boolean bottom) {
		mMovedThroughTop = top;
		mMovedThroughBot = bottom;
	}
	
	public boolean getMovedThroughTop() {
		return mMovedThroughTop;
	}
	
	public boolean getMovedThroughBot() {
		return mMovedThroughBot;
	}
	
	public void setHoldingStatus(boolean isHeld) {
		mIsHeld = isHeld;
	}
	
	public boolean isHeld() {
		return mIsHeld;
	}
	
	/**
	 * Updates the position of the ball based on its velocity.
	 */
	public void updatePosition() {
		float newPosX = mCenter.X() + mVelocity.X();
		float newPosY = mCenter.Y() + mVelocity.Y();
		float newPosZ = mCenter.Z() + mVelocity.Z();
		mCenter.set(newPosX, newPosY, newPosZ);
		if (mRecording) {
			mRecordedPos.add(new Vector(mCenter.X(), mCenter.Y(), mCenter.Z()));
		}
		if (!mIsHeld) {
			// Handles friction in the xz-plane if the ball is rolling on the ground.
			if (mRolling) {
				Vector newV = new Vector(mVelocity.X() * FRICTION, mVelocity.Y(), mVelocity.Z() * FRICTION);
				if (newV.magnitude() < FRICTION_THRESHOLD) {
					newV.set(0, mVelocity.Y(), 0);
					mRolling = false;
				}
				mVelocity = newV;
				mRecording = false;
			}
			else { 
				// Handles bounce motion in the y direction.
				float newVy = mVelocity.Y() + GRAVITY;
				mVelocity.setY(newVy);
			}
		}
	}
	
	public ArrayList<Vector> getRecordedValues() {
		return mRecordedPos;
	}

	/**
	 * Detects and handles a collision with the ground.  The ground should be a flat plane 
	 * on the xz axis.
	 * @param floorY The y-position of the floor.
	 */
	public boolean handleFloorCollision(float floorY) {
		if (mCenter.Y() <= (floorY + mRadius) && mVelocity.Y() < 0) { 
			mCenter.setY(floorY + mRadius);
			if (Math.abs(mVelocity.Y()) < BOUNCE_THRESHOLD) {
				mVelocity.setY(0);
				mRolling = true;
			}
			else {
				mVelocity.setY(-mVelocity.Y() * LOSS_FLOOR_ENERGY);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Detects and handles a collision with the backboard.
	 * @param center The center location of the backboard
	 * @param width The width of the backboard
	 * @param height The height of the backboard.
	 * @return true if a collision occurred.
	 */
	public boolean handleBackboardCollision(Vector center, float width, float height) {
		boolean isCollidingX = false;
		boolean isCollidingY = false;
		boolean isCollidingZ = false;
		if ((mCenter.X() + mRadius) >= (center.X() - width/2.0) &&
				(mCenter.X() - mRadius) <= (center.X() + width/2.0)) {
			isCollidingX = true;
		}
		if ((mCenter.Y() + mRadius) >= (center.Y() - height/2.0) &&
				mCenter.Y() - mRadius <= (center.Y() + height/2.0)) {
			isCollidingY = true;
		}
		if (center.Z() > (mCenter.Z() - mRadius) && center.Z() < (mCenter.Z() + mRadius)) {
			isCollidingZ = true;
		}
		
		if (isCollidingX && isCollidingY && isCollidingZ) {
			mCenter.set(mCenter.X(), mCenter.Y(), center.Z() + mRadius);
			mVelocity.setZ(-mVelocity.Z() * LOSS_GOAL_ENERGY);
			return true;
		}
		return false;
		
	}
	
	/**
	 * Detects and handles a collision with a given ring (torus).  Used to bounce the ball off of 
	 * the rim if it hit.
	 * @param ring The ring (torus) that the ball may be colliding with.
	 * @return returns true if a collision occurred.
	 */
	public boolean handleRimCollision(Ring ring) {
		if (ring.isColliding(this)) {
			Vector contactPoint = ring.findClosestPoint(mCenter);
			Vector normal = mCenter.subtract(contactPoint);
			normal.normalize();
			double mag = mVelocity.magnitude();
			Vector newV = normal.multiplyScalar((float)mag);
			newV.set(newV.X() * LOSS_GOAL_ENERGY, newV.Y() * LOSS_GOAL_ENERGY, newV.Z() * LOSS_GOAL_ENERGY);
			mVelocity = newV;
			return true;
		}
		return false;
	}
	
	/**
	 * Checks to see if this ball contains the given point.
	 * @param point
	 * @return true if the ball contains the point, false otherwise.
	 */
	public boolean contains(Vector point) {
		float distX = mCenter.X() - point.X();
		float distY = mCenter.Y() - point.Y();
		float distZ = mCenter.Z() - point.Z();
		double dist = Math.sqrt(Math.pow(distX,2) + Math.pow(distY,2) + Math.pow(distZ,2));
		if (dist <= mRadius) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param on
	 */
	public boolean checkOutOfBounds(float boundsUp, float boundsDown, float boundsSide) {
		if (mCenter.X() < -boundsSide || mCenter.X() > boundsSide) {
			return true;
		}
		if (mCenter.Z() > boundsUp || mCenter.Z() < boundsDown) {
			return true;
		}
		return false;
	}
	
	public void setReplaying(boolean on) {
		if (!mRecording) {
			mReplaying = on;
			if (!on) {
				mPlayback.clear();
			}
		}
	}
	
	public boolean getReplaying() {
		return mReplaying;
	}
	
	public void setRecording(boolean recording){
		mRecording = recording;
	}
	
	public boolean getRecording() {
		return mRecording;
	}
	
	public ArrayList<Vector> interpolate(ReplaySpeed speed) {
		mPlayback = new ArrayList<Vector>();
		if (speed == ReplaySpeed.NORMAL) {
			for (int i = 0; i < (mRecordedPos.size()); ++i) {
				Vector cur = mRecordedPos.get(i);
				mPlayback.add(new Vector(cur.X(), cur.Y(), cur.Z()));
			}
		}
		else if (speed == ReplaySpeed.SLOW) {
			for (int i = 0; i < (mRecordedPos.size()); ++i) {
				Vector cur = mRecordedPos.get(i);
				mPlayback.add(new Vector(cur.X(), cur.Y(), cur.Z()));
				for (int j = 0; j < 3; ++j) {
					Vector newPos = new Vector(cur.X(), cur.Y(), cur.Z());
					mPlayback.add(newPos);
				}
			}
		}
		else if (speed == ReplaySpeed.VERY_SLOW) {
			for (int i = 0; i < (mRecordedPos.size()); ++i) {
				Vector cur = mRecordedPos.get(i);
				mPlayback.add(new Vector(cur.X(), cur.Y(), cur.Z()));
				for (int j = 0; j < 9; ++j) {
					Vector newPos = new Vector(cur.X(), cur.Y(), cur.Z());
					mPlayback.add(newPos);
				}
			}
		}
		return mPlayback;
	}
	
	/**
	 * Resets the scoring status of this ball, e.g. whether it passed through the 
	 * boundaries of the hoop or not.
	 */
	public void resetScoreStatus() {
		mMovedThroughTop = false;
		mMovedThroughBot = false;
	}
	
	/**
	 * Resets the ball to the starting state.
	 * @param x The center x position of the ball.
	 * @param y The center y position of the ball.
	 * @param z The center z position of the ball.
	 */
	public void reset(float x, float y, float z) {
		resetScoreStatus();
		mIsHeld = true;
		mRolling = false;
		mVelocity.set(0, 0, 0);
		mCenter.set(x, y, z);
		mReplaying = false;
		mRecordedPos.clear();
		mRecordedPos.add(mCenter);
		if (mPlayback != null) {
			mPlayback.clear();
		}
		mRecording = false;
	}
}
