package com.example.virtualbasketball;

/**
 * This class is the data representation for a 3D torus.  It is used to represent the basketball 
 * rim, and keeps track of its position, size, and detects scoring and collisions with the ball.
 * @author Megan Worley
 *
 */
public class Ring extends Geometry {
	private Vector[] mCollisionCircle;
	private float mInnerRadius;
	private float mOuterRadius;
	private float mHeight;
	private static final int NUM_POINTS = 60;
	
	/**
	 * Default constructor.
	 * @param center 
	 * @param scale
	 * @param innerRadius
	 * @param outerRadius
	 * @param height
	 */
	public Ring(Vector center, float innerRadius, float outerRadius, float height) {
		super(center);
		mInnerRadius = innerRadius;
		mOuterRadius = outerRadius;
		mHeight = height;
		createCollisionCircle();
	}
	
	/**
	 * This method initializes the circle used for calculating the direction the ball will bounce off 
	 * of the rim.  It is fixed on the xz plane.
	 */
	private void createCollisionCircle() {
		mCollisionCircle = new Vector[NUM_POINTS];
		float r = (float)((mInnerRadius + mOuterRadius) / 2.0);
		
		// theta is from 0 to 2*pi. 
		for (int i = 0; i < NUM_POINTS; ++i) {
			float theta = (float)(i*2*Math.PI/NUM_POINTS);
			float x = (float)(r*Math.cos(theta));
			float y = 0;
			float z = (float)(r*Math.sin(theta));
			Vector point = new Vector(x + mCenter.X(), y + mCenter.Y(), z + mCenter.Z());
			mCollisionCircle[i] = point;
		}
	}
		
	/**
	 * Checks to see if this is colliding with a ball.  Used to determine whether the basketball has hit 
	 * the hoop or not.
	 * @param ball
	 * @return
	 */
	public boolean isColliding(Ball ball) {
		float xdist = ball.getCenter().X() - mCenter.X();
		float ydist = ball.getCenter().Y() - mCenter.Y();
		float zdist = ball.getCenter().Z() - mCenter.Z();
		float distBallToRing = (float)(Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2) + Math.pow(zdist, 2)));
		if (distBallToRing - ball.getRadius() > mOuterRadius) {
			return false;
		}
		float yupper = ball.getCenter().Y() + ball.getRadius();
		float ylower = ball.getCenter().Y() - ball.getRadius();
		float innerDist = distBallToRing + ball.getRadius();
		
		if (innerDist >= mInnerRadius 
				&& ylower < mCenter.Y() + mHeight / 2
				&& yupper > mCenter.Y() - mHeight / 2) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Determines the closest point on the collision ring to the given point.
	 * @param point 
	 * @return
	 */
	public Vector findClosestPoint(Vector point) {
		Vector nearest = mCollisionCircle[0];
		float nearestDist = (float)((nearest.subtract(point)).magnitude());
		for (int i = 1; i < NUM_POINTS; i++) {
			float dist = (float)((mCollisionCircle[i].subtract(point)).magnitude());
			if (dist < nearestDist) {
				nearest = mCollisionCircle[i];
				nearestDist = dist;
			}
		}
		return nearest;
	}
	
	/**
	 * This method checks to see if a basketball passed through this hoop.
	 * @param ball The basketball that was shot.
	 * @return true if a point was scored, false otherwise
	 */
	public boolean checkScoring(Ball ball) {
		// Upper boundary.
		float yUpper = mCenter.Y() + mHeight/2.0f + ball.getRadius();
		// Lower boundary.
		float yLower = mCenter.Y() - mHeight/2.0f - ball.getRadius();
		
		// Determines the distances to the boundaries in all 3 dimensions.
		float yUpperDist = Math.abs(ball.getCenter().Y() - yUpper);
		float yLowerDist = Math.abs(ball.getCenter().Y() - yLower);
		float xDist = ball.getCenter().X() - mCenter.X();
		float zDist = ball.getCenter().Z() - mCenter.Z();
		double xzDist = Math.sqrt(Math.pow(xDist, 2) + Math.pow(zDist, 2));
		
		// Check to see if the ball passed through the upper boundary or the lower boundary of the 
		// hoop space.
		if ((yUpperDist < ball.getRadius()) && mOuterRadius >= (xzDist + ball.getRadius())) {
			ball.setMovedThroughHoop(true, ball.getMovedThroughBot());
		}
		if ((yLowerDist < ball.getRadius()) && mOuterRadius >= xzDist) {
			ball.setMovedThroughHoop(ball.getMovedThroughTop(), true);
		}
		
		// If the ball passed through both the top boundary and the bottom boundary, then we 
		// know a point was scored.
		if (ball.getMovedThroughTop() && ball.getMovedThroughBot()) {
			ball.resetScoreStatus();
			return true;
		}
		return false;
	}
}
