package com.example.virtualbasketball;


import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

/**
 * This class is the custom view for the program.  It shows the 3D model (drawn by the renderer) 
 * and allows the user to interact with it.
 * @author Megan Worley
 */
public class CustomGLSurfaceView extends GLSurfaceView {
	private MyRenderer mRenderer;
	private static final float MOVE_FACTOR = 1/5f;
	private float mPreviousX;
	private float mPreviousY;
	
	/**
	 * Default constructor
	 * @param context The activity instance this view is associated 
	 * with (ViewerActivity).
	 */
	public CustomGLSurfaceView(Context context) {
		super(context);
		initialize();
	}
	
	/**
	 * Constructor that instantiates the view from a layout.
	 * @param context
	 * @param attrs
	 */
	public CustomGLSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}
	
	private void initialize() {
		mPreviousX = 0;
		mPreviousY = 0;
		setEGLContextClientVersion(2);
		setEGLConfigChooser(new GLSurfaceView.EGLConfigChooser() {
			public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
				// Ensure that we get a 16bit framebuffer. Otherwise, we'll fall
				// back to Pixelflinger on some device (read: Samsung I7500)
				int[] attributes = new int[] { EGL10.EGL_DEPTH_SIZE, 16, EGL10.EGL_NONE };
				EGLConfig[] configs = new EGLConfig[1];
				int[] result = new int[1];
				egl.eglChooseConfig(display, attributes, configs, 1, result);
				return configs[0];
			}
		});
		
		mRenderer = new MyRenderer(getContext());
		setRenderer(mRenderer);
		setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
	}

	public MyRenderer getRenderer() {
		return mRenderer;
	}
	
	
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		float x = e.getX();
		float y = e.getY();
		
			switch (e.getAction()) {
			case MotionEvent.ACTION_MOVE:
				float dx = x - mPreviousX;
				float dy = y - mPreviousY;
				mRenderer.panCamera(-dx * MOVE_FACTOR, dy * MOVE_FACTOR);
			}
		
		mPreviousX = x;
		mPreviousY = y;
		
		return true;
	}
}
