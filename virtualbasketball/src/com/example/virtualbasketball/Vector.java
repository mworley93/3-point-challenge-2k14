package com.example.virtualbasketball;

/**
 * This class represents a 3D vector.
 * @author Megan Worley
 */
public class Vector {
	private float x;
	private float y; 
	private float z;
	
	/**
	 * Default constructor.
	 */
	public Vector() {
		x = 0;
		y = 0;
		z = 0;
	}
	
	/**
	 * Constructor.
	 * @param x The x value.
	 * @param y The y value.
	 * @param z The z value.
	 */
	public Vector(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	

	// These functions get the individual coordinates.
	
	public float X() {
		return x;
	}
	public float Y() {
		return y;
	}
	public float Z() {
		return z;
	}
	
	// Sets the coordinate values for this vector.
	public void set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public void setZ(float z) {
		this.z = z;
	}
	
	/**
	 * Computes the magnitude of this vector.
	 * @return The magnitude
	 */
	public double magnitude() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
	}
	
	/**
	 * Normalizes this vector.
	 */
	public void normalize() {
		double mag = magnitude();
		x = (float)(x/mag);
		y = (float)(y/mag);
		z = (float)(z/mag);
	}
	
	/**
	 * Subtracts a given vector from this one.
	 * @param v1
	 * @param v2
	 * @return v1 - v2
	 */
	public Vector subtract(Vector v2) {
		return new Vector(x - v2.X(), y - v2.Y(), z - v2.Z());
	}
	
	public float dot(Vector v2) {
		return (x * v2.X() + y*v2.Y() + z*v2.Z());
	}
	
	public Vector cross(Vector v2) {
		float i = this.y * v2.Z() - this.z * v2.Y();
		float j = this.x * v2.Z() - this.z * v2.X();
		float k = this.x * v2.Y() - this.y * v2.X();
		return new Vector(i, -j, k);
	}
	
	public Vector multiply(Vector v2) {
		return new Vector(x * v2.X(), y* v2.Y(), z*v2.Z());
	}

	public Vector multiplyScalar(float c) {
		return new Vector(x*c, y*c, z*c);
	}
	
	@Override
	public String toString() {
		return ("x: " + x + " y: " + y + " z: " + z);
	}
	
	public Vector copy() {
		return new Vector(x, y, z);
	}
}
