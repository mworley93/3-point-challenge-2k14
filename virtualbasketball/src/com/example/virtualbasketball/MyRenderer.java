package com.example.virtualbasketball;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;

import com.example.test2_3d.R;
import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.Light;
import com.threed.jpct.Loader;
import com.threed.jpct.Logger;
import com.threed.jpct.Matrix;
import com.threed.jpct.Object3D;
import com.threed.jpct.Primitives;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;
import com.threed.jpct.util.BitmapHelper;
import com.threed.jpct.util.MemoryHelper;
import com.threed.jpct.util.SkyBox;

public class MyRenderer implements GLSurfaceView.Renderer {

	private Ball mBasketball;
	private Ring mRim;

	private final float FLOOR_Y = -1;
	private final Vector BACKBOARD_POSITION = new Vector(0, 158, -1356);
	private final float BACKBOARD_WIDTH = 126;
	private final float BACKBOARD_HEIGHT = 90;
	private final Vector RIM_POSITION = new Vector(0, 141, -1331);
	private final float RIM_INNER_R = 15;
	private final float RIM_OUTER_R = 18;
	private final float RIM_HEIGHT = 4;
	private final float PLAYER_BOUNDS_RIGHT = 10f;
	private final float PLAYER_BOUNDS_LEFT = -10f;
	private final float AIM_BOUNDS_LEFT = -0.3f;
	private final float AIM_BOUNDS_RIGHT = 0.3f;
	private final float AIM_BOUNDS_UP = 0.4f;
	private final float AIM_BOUNDS_DOWN = 0;
	private final float VIEW_ANGLE_CHANGE = 0.01f;
	private final float COURT_BOUNDS_UP = 10;
	private final float COURT_BOUNDS_SIDE = 400;
	private final float COURT_BOUNDS_DOWN = -1550;

	public float mAngleX;
	public float mAngleY;
	
	public Camera cam;
	private SimpleVector mCenterOfView;
	private ArrayList<Vector> mReplayValues;
	private int mReplayIndex;

	private boolean mReady = false;
	
	private static MainActivity master = null;

	private FrameBuffer fb = null;
	private World world = null;
	private RGBColor back = new RGBColor(50, 50, 100);

	private int fps = 0;
	private long lastTime = 0;

	private Light sun = null;
	private SkyBox skybox = null;
	
	private Object3D court;
	private Object3D hoops;
	private Object3D ball;
	private Object3D plane;
	private String objectName = "basketball_court_raw";
	private float objectScale = 0.2f;
	private long time = System.currentTimeMillis();
	
	Context mContext = null;

	public MyRenderer(Context context) {
		mContext = context;
	}

	public void onSurfaceChanged(GL10 gl, int w, int h) {
		if (fb != null) {
			fb.dispose();
		}
		fb = new FrameBuffer(w, h);

		if (master == null) {

			world = new World();
			world.setAmbientLight(20, 20, 20);

			sun = new Light(world);
			sun.setIntensity(450, 450, 450);
			
			// Load textures.
			Texture courtTexture = loadTexture(R.drawable.basketball_floor, "courtTexture");
			Texture ballTexture = loadTexture(R.drawable.basketball_texture, "basketballTexture");
			loadTexture(R.drawable.basketball_crowd_flipped, "front");
			loadTexture(R.drawable.basketball_crowd_side_flipped, "left");
			loadTexture(R.drawable.basketball_crowd_side_flipped, "right");
			loadTexture(R.drawable.basketball_crowd_flipped, "back");
			loadTexture(R.drawable.basketball_ceiling, "down");
			loadTexture(R.drawable.dark_wood_small, "up");
			// Create skybox.
			skybox = new SkyBox(3000);
			// Load the models.
			InputStream istream = null;
			try {
				istream = mContext.getAssets().open("basketball_floor_raw.3ds");
				court = loadModel(istream, objectScale);
				
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			try {
				istream = mContext.getAssets().open("basketball" + ".3ds");
				ball = loadModel(istream, 1.0f);
				
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			try {
				istream = mContext.getAssets().open("basketball_hoops.3ds");
				hoops = loadModel(istream, objectScale);
				
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			// Set the textures.
			if (courtTexture != null) {
				court.setTexture("courtTexture");
			}
			if (ballTexture != null) {
				ball.setTexture("basketballTexture");
			}
			
			court.build();
			world.addObject(court);
			ball.build();
			world.addObject(ball);
			hoops.build();
			world.addObject(hoops);
			
			cam = world.getCamera();
			world.setClippingPlanes(1.0f, 2000.0f);

			SimpleVector sv = new SimpleVector();
			sv.set(court.getTransformedCenter());
			sv.y += 100;
			sv.z += 100;
			sun.setPosition(sv);
			MemoryHelper.compact();

			if (master == null) {
				Logger.log("Saving master Activity!");
				master = (MainActivity) mContext;
			}
			
			createGeometry();
		}
	}
	
	private Texture loadTexture(int resource, String textureName) {
		InputStream istream = null;
		Texture texture= null;
		try {
			istream = mContext.getResources().openRawResource(resource);
			Bitmap textureBitmap = BitmapHelper.loadImage(istream);
			textureBitmap = BitmapHelper.rescale(textureBitmap, 1024, 1024);
			texture = new Texture(textureBitmap);
			TextureManager.getInstance().addTexture(textureName, texture);
		}
		catch(Exception e) {
			
		}
		return texture;
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
	}
	
	private Object3D loadModel(InputStream filename, float scale) {
        Object3D[] model = Loader.load3DS(filename, scale);
        Object3D o3d = new Object3D(0);
        Object3D temp = null;
        for (int i = 0; i < model.length; i++) {
            temp = model[i];
            temp.setCenter(SimpleVector.ORIGIN);
            temp.rotateX((float)( -.5*Math.PI));
            temp.rotateZ((float)(Math.PI));
            temp.rotateMesh();
            temp.setRotationMatrix(new Matrix());
            o3d = Object3D.mergeObjects(o3d, temp);
            o3d.build();
        }
        return o3d;
    }
	
	/**
	 * This method is the drawing method for each render frame.
	 */

	public void onDrawFrame(GL10 gl) {
		transformBall();
		if (mBasketball.checkOutOfBounds(COURT_BOUNDS_UP, COURT_BOUNDS_DOWN, 
				COURT_BOUNDS_SIDE)) {
			ball.setTransparency(0);
		}
		else {
			ball.setTransparency(100);
		}
		update();

		fb.clear(back);
		skybox.render(world, fb);
		world.renderScene(fb);
		world.draw(fb);
		fb.display();

		if (System.currentTimeMillis() - time >= 1000) {
			Logger.log(fps + "fps");
			fps = 0;
			time = System.currentTimeMillis();
		}
		fps++;
	}
	
	/**
	 * Initializes the geometric objects that will be used.
	 */
	public void createGeometry() {
		mRim = new Ring(new Vector(RIM_POSITION.X(),RIM_POSITION.Y(),RIM_POSITION.Z()), 
				RIM_INNER_R, RIM_OUTER_R, RIM_HEIGHT);
		cam.setPosition(court.getCenter().x, court.getCenter().y + 100, 
				8 * court.getCenter().z / 5);
		mCenterOfView = new SimpleVector(0,0,-1);
		cam.setOrientation(mCenterOfView, new SimpleVector(0,1,0));
		mAngleX = 0f;
		mAngleY = 0f;
		mReady = true;
		ball.scale(0.20f);
		Vector restVec = calculateRestingSpot();
		mBasketball = new Ball(new Vector(cam.getPosition().x + restVec.X()*28f, 
	    		cam.getPosition().y + restVec.Y()*28f, cam.getPosition().z 
	    		+ restVec.Z() * 28f), 7.5f);
	}
	
	private void transformBall() {
		SimpleVector back = ball.getTransformedCenter();
		back.scalarMul(-1);
		ball.translate(back);
		ball.translate(new SimpleVector(mBasketball.getCenter().X(), mBasketball.getCenter().Y(), 
				mBasketball.getCenter().Z()));
	}
	
	/*
	 * 
	 * This function plays audio content located in the assets folder.
	 * 
	 */
	private void play(Context context, String file) {
		try {
		    AssetFileDescriptor afd = context.getAssets().openFd(file);
		    MediaPlayer mediaPlayer = new MediaPlayer();
		    mediaPlayer.setDataSource(
		            afd.getFileDescriptor(),
		            afd.getStartOffset(),
		            afd.getLength()
		        );
		    afd.close();
		    
		    mediaPlayer.prepare();
		    mediaPlayer.start();
		} catch (IllegalArgumentException e) {
		    e.printStackTrace();
		} catch (IllegalStateException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}


	/**
	 * This method updates the position of the basketball.
	 */
	private void update() {
		if (!mBasketball.getReplaying()) {
			mBasketball.updatePosition();
			if (mBasketball.handleFloorCollision(FLOOR_Y) && mBasketball.getVelocity().Y() != 0) {
				
				// TODO: Play sound of basketball hitting the floor.
				long time = System.currentTimeMillis();
				
				int num = (int)(Math.random() * (3));
				if(lastTime == 0) {
					lastTime = time;
					if (num == 0) {
						play(mContext, "basketball_bounce1.mp3");
					}
					else if (num == 1) {
						play(mContext, "basketball_bounce2.mp3");
					}
					else if (num == 2){
						play(mContext, "basketball_bounce3.mp3");
					}
				}
				else if (time - lastTime < 500 ) {
					// wait...
				}
				else {
					lastTime = time;
					if (num == 0) {
						play(mContext, "basketball_bounce1.mp3");
					}
					else if (num == 1) {
						play(mContext, "basketball_bounce2.mp3");
					}
					else if (num == 2){
						play(mContext, "basketball_bounce3.mp3");
					}
				}
				

			}
			if (mBasketball.handleBackboardCollision(BACKBOARD_POSITION, BACKBOARD_WIDTH, 
					BACKBOARD_HEIGHT)) {
				// TODO: Play sound of basketball hitting the backboard.
//				long time = System.currentTimeMillis();
//				if(lastTime == 0) {
//					lastTime = time;
//					play(mContext, "basketball_backboard.mp3");
//				}
//				else if (time - lastTime < 500 ) {
//					// wait...
//				}
//				else {
//					lastTime = time;
//					play(mContext, "basketball_backboard.mp3");
//				}
				
			}
			if (mBasketball.handleRimCollision(mRim)) {
				// TODO: Play sound of basketball hitting the rim.
				long time = System.currentTimeMillis();
				if(lastTime == 0) {
					lastTime = time;
					play(mContext, "basketball_rim.mp3");
				}
				else if (time - lastTime < 200 ) {
					// wait...
				}
				else {
					lastTime = time;
					play(mContext, "basketball_rim.mp3");
				}
				
			}
			if (mRim.checkScoring(mBasketball)) {
				// TODO: Play sound of scoring.
				int num = (int)(Math.random() * (2));
				
				if (num == 0) {
					play(mContext, "basketball_swish1.mp3");
				}
				else if (num == 1) {
					play(mContext, "basketball_swish2.mp3");
				}
				play(mContext, "basketball_game.mp3");
			}
		}
		else {
			if (mReplayIndex < mReplayValues.size()) {
				mBasketball.setCenter(mReplayValues.get(mReplayIndex).X(), 
						mReplayValues.get(mReplayIndex).Y(), mReplayValues.get(mReplayIndex).Z());
				mReplayIndex += 1;
			}
			else {
				mBasketball.setReplaying(false);
			}
		}
	}
		
		/**
		 * This method is called to change of camera direction or position when the arrow buttons 
		 * are pressed.  Whether the viewing area is limited or not depends on if the player has 
		 * shot the ball already or not.
		 * @param change How the viewpoint should be changed (defined in ViewpointChange.java).
		 */
		public void changeCamera(ViewpointChange change) {
			if (mBasketball.isHeld()) {
				changeFixedPerspective(change);
			}
			else {
				changeFreePerspective(change);
			}
		}
		
		/**
		 * This method controls how the perspective changes while the player is fixed at the 
		 * free throw line.
		 * @param change The desired viewpoint change, determined by which arrow button they pressed.
		 */
		private void changeFixedPerspective(ViewpointChange change) {
				switch (change) {
				case VIEW_UP:
					if ((mAngleY + VIEW_ANGLE_CHANGE) < AIM_BOUNDS_UP) {
						rotateCenterOfView( (float)(-VIEW_ANGLE_CHANGE), cam.getSideVector().x, 
								cam.getSideVector().y, cam.getSideVector().z);
						mAngleY += VIEW_ANGLE_CHANGE;
					}
					break;
				case VIEW_LEFT:
					if ((mAngleX - VIEW_ANGLE_CHANGE) > AIM_BOUNDS_LEFT) {
						rotateCenterOfView( (float)(-VIEW_ANGLE_CHANGE), 0, 1, 0);
						mAngleX -= VIEW_ANGLE_CHANGE;
					}
					break;
					
				case VIEW_DOWN:
					if ((mAngleY - VIEW_ANGLE_CHANGE) > AIM_BOUNDS_DOWN) {
						rotateCenterOfView( (float)(VIEW_ANGLE_CHANGE), cam.getSideVector().x, 
								cam.getSideVector().y, cam.getSideVector().z);
						mAngleY -= VIEW_ANGLE_CHANGE;
					}
					break;
				case VIEW_RIGHT:
					if ((mAngleX + VIEW_ANGLE_CHANGE) < AIM_BOUNDS_RIGHT) {
						rotateCenterOfView( (float)(VIEW_ANGLE_CHANGE), 0, 1, 0);
						mAngleX += VIEW_ANGLE_CHANGE;
					}
					break;
				case MOVE_LEFT:
					if ((cam.getPosition().x - 0.5f) > PLAYER_BOUNDS_LEFT) {
						cam.setPosition((float)(cam.getPosition().x - 0.5f),
								cam.getPosition().y, cam.getPosition().z);
					}
					break;
				case MOVE_RIGHT:
					if ((cam.getPosition().x + 0.5f) < PLAYER_BOUNDS_RIGHT) {
						cam.setPosition((float)(cam.getPosition().x + 0.5f),
								cam.getPosition().y, cam.getPosition().z);
					}
					break;
			}
				Vector restVec = calculateRestingSpot();
				mBasketball.setCenter(cam.getPosition().x + restVec.X()*28f, 
			    		cam.getPosition().y + restVec.Y()*28f, cam.getPosition().z 
			    		+ restVec.Z() * 28f);
		}
		
		/**
		 * 
		 * @param change
		 */
		private void changeFreePerspective(ViewpointChange change) {
			switch(change) {
			case VIEW_UP:
					rotateCenterOfView( (float)(-Math.PI/60), cam.getSideVector().x, 
							cam.getSideVector().y, cam.getSideVector().z);
				break;
			case VIEW_LEFT:
					rotateCenterOfView( (float)(-Math.PI/60), 0, 1, 0);
					mAngleX -= Math.PI/60;
				break;
				
			case VIEW_DOWN:
					rotateCenterOfView( (float)(Math.PI/60), cam.getSideVector().x, 
							cam.getSideVector().y, cam.getSideVector().z);
					mAngleY -= Math.PI/60;
				break;
			case VIEW_RIGHT:
					rotateCenterOfView( (float)(Math.PI/60), 0, 1, 0);
					mAngleX += Math.PI/60;
				break;
			}
		}
		
		public void rotateCenterOfView(float angle, float x, float y, float z) {
			mCenterOfView.rotateAxis(new SimpleVector(x,y,z), angle);
			mCenterOfView.normalize();
			SimpleVector up = cam.getUpVector();
			up.rotateAxis(new SimpleVector(x,y,z), angle);
			cam.setOrientation(mCenterOfView, up);
		}
		
		public void panCamera(float dx, float dy) {
			if (!mBasketball.isHeld()) {
				int modeX = 0;
				if (dx < 0) {
					modeX = Camera.CAMERA_MOVELEFT;
				}
				else {
					modeX = Camera.CAMERA_MOVERIGHT;
				}
				int modeY = 0;
				if (dy < 0) {
					modeY = Camera.CAMERA_MOVEDOWN;
				}
				else {
					modeY = Camera.CAMERA_MOVEUP;
				}
				
				cam.moveCamera(modeX, Math.abs(dx));
				cam.moveCamera(modeY, Math.abs(dy));
			}
		}
		
		public void throwBall(float speed) {
			if (mReady && mBasketball.isHeld()) {
				Vector direction = calculateShotVector();
				Vector velocity = direction.multiplyScalar(speed);
				mBasketball.setVelocity(velocity.X(), velocity.Y() * 1.2f, velocity.Z());
				mBasketball.setHoldingStatus(false);
				mBasketball.setRecording(true);
			}
		}
		
		public Vector calculateShotVector() {
			SimpleVector shotVector = cam.getDirection();
			shotVector.rotateX(0.70f);
			Vector shot = new Vector(shotVector.x, shotVector.y, shotVector.z);
			return shot;
		}
		
		public Vector calculateRestingSpot() {
			SimpleVector restingVector = cam.getDirection();
			restingVector.rotateX(-0.5f);
			Vector restPos = new Vector(restingVector.x, restingVector.y, restingVector.z);
			return restPos;
		}
		
		public void replay(ReplaySpeed speed) {
			mReplayValues = mBasketball.interpolate(speed);
			mBasketball.setReplaying(true); 
			mReplayIndex = 0;
		}
		
		public void reset() {
			cam.setPosition(court.getCenter().x, court.getCenter().y + 100, 
					8 * court.getCenter().z / 5);
			mCenterOfView = new SimpleVector(0,0,-1);
			cam.setOrientation(mCenterOfView, new SimpleVector(0,1,0));
			mAngleX = 0f;
			mAngleY = 0f;
			mReady = true;
			Vector restVec = calculateRestingSpot();
			mBasketball.reset(cam.getPosition().x + restVec.X()*28f, 
		    		cam.getPosition().y + restVec.Y()*28f, cam.getPosition().z 
		    		+ restVec.Z() * 28f);
		}
}
