package com.example.virtualbasketball;


/**
 * @author Megan Worley
 */
public enum ReplaySpeed {
    NORMAL (1),
    SLOW   (0.25f),
    VERY_SLOW   (0.1f);
    
    private final float speed;
    
    ReplaySpeed(float speed) {
        this.speed = speed;
    }
    
    public float value() {
        return speed;
    }
    
    public ReplaySpeed increase() {
    	ReplaySpeed higher = null;
    	if (speed == NORMAL.value() || speed == SLOW.value()) {
    		higher = NORMAL;
    	}
    	else {
    		higher = SLOW;
    	}
    	return higher;
    }
    
    public ReplaySpeed decrease() {
    	ReplaySpeed lower = null;
    	if (speed == VERY_SLOW.value() || speed == SLOW.value()) {
    		lower = VERY_SLOW;
    	}
    	else {
    		lower = SLOW;
    	}
    	return lower;
    }
}