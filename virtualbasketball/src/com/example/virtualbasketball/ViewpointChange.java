package com.example.virtualbasketball;

public enum ViewpointChange {
	VIEW_UP, 
	VIEW_DOWN,
	VIEW_LEFT,
	VIEW_RIGHT,
	MOVE_LEFT,
	MOVE_RIGHT
}
